package zombicide;

import java.util.ArrayList;

public class Hacha extends Arma {
	public Hacha(String nombre, int dano, int alcance, int acierto, int usos_habilidad_especial) {
		super(nombre, dano, alcance, acierto, usos_habilidad_especial);
	}
	//FUNCIÓN DE LA HABILIDAD ESPECIAL: CUANDO SE ACTIVA ELIMINA A UN ZOMBIE GORDO
	public ArrayList<Zombie> ataque_especial(ArrayList<Zombie> zombies) {
		int contador = 0;
		for (int i = 0; i < zombies.size() && contador < 1; i++) {
			if (zombies.get(i).getTipo() == 2) {
				zombies.remove(i);
				contador++;
			}
		}
		contador = 1;
		return zombies;
	}
}