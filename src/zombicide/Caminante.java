package zombicide;

import java.util.ArrayList;

public class Caminante extends Zombie{
	
	// Constructor para un Caminante con parámetros personalizados
	public Caminante(String nombre, int salud, int maxSalud, boolean vivo, int movimiento, int dano, int tipo) {
		super(nombre, salud, maxSalud, vivo, movimiento, dano, tipo);
	}

	// Constructor para un Caminante simple (con valores por defecto)
	public Caminante(boolean vivo) {
		super("Caminante", 1, 1, vivo, 1, 1, 1);
	}
	
	// Retorna la cantidad de caminantes enemigos.
	public int numero_caminantes(ArrayList<Zombie> zombies) {
		int contador = 0;
		for (int i = 0; i < zombies.size(); i++) {
			if (zombies.get(i).getTipo() == 1) {
				contador++;
			}
		}
		return contador;
	}
	
	// Añadimos al ArrayList de zombies enemigos la misma cantidad de caminantes que ya tenemos, duplicándolos.
	public ArrayList<Zombie> habilidad_especial(ArrayList<Zombie> zombies){
		int cantidad_nuevos_caminantes = numero_caminantes(zombies);
		for (int i = 0; i < cantidad_nuevos_caminantes; i++) {
			zombies.add(new Caminante(true));
		}
		return zombies;
	}
}
