package zombicide;

public class Humanoide {
    private String nombre;
    private int salud;
    private int maxSalud;
    private boolean vivo;

    // Constructor de la clase Humanoide
    public Humanoide(String nombre, int salud, int maxSalud, boolean vivo) {
        setNombre(nombre);
        setSalud(salud);
        setMaxSalud(maxSalud);
        setVivo(vivo);
    }

    // Métodos getter y setter para el nombre
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    // Método para disminuir la salud
    public int getSalud() {
        return salud;
    }

    public void setSalud(int dano) {
        this.salud -= dano;
    }

    // Método para restaurar la salud al máximo
    public void setRestaurarSalud() {
        this.salud = maxSalud;
    }

    // Métodos getter y setter para la salud máxima
    public int getMaxSalud() {
        return maxSalud;
    }

    public void setMaxSalud(int maxSalud) {
        this.maxSalud = maxSalud;
    }

    // Métodos getter y setter para el estado de vida
    public boolean isVivo() {
        return vivo;
    }

    public void setVivo(boolean vivo) {
        this.vivo = vivo;
    }

    // Método toString para representar el objeto Humanoide como cadena de texto
    public String toString() {
        return "nombre = " + getNombre() + ", salud = " + getSalud() + ", maxSalud = " + getMaxSalud();
    }
}
