package zombicide;

import java.util.ArrayList;
import java.util.Scanner;

public class Zombicide {
	protected static ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
	protected static ArrayList<Jugador> jugadores_seleccionados = new ArrayList<Jugador>();
	protected static ArrayList<Zombie> zombies = new ArrayList<Zombie>();
	private static ArrayList<Arma> armas = new ArrayList<Arma>();
	
	public static void main(String[] args) {
		init_zombies();
		init_personajes();
		init_objetos();
		menuZombicide();
	}
	
	// Nuestro menu.
	public static void menuZombicide() {
		Scanner leer = new Scanner(System.in);
		int opcion;
		do {
			System.out.println("\r _______________________________          ▄▄▄▄▄▄▄▄▄"); 
			System.out.println("|  ___________________________  |         ▌▐░▀░▀░▀▐");
			System.out.println("| |                           | |         ▌░▌░░░░░▐");
			System.out.println("| |         \u001B[31mZOMBICIDE\u001B[0m         | |         ▌░░░░░░░▐");
			System.out.println("| |                           | |         ▄▄▄▄▄▄▄▄▄");
			System.out.println("| |   1. Nueva partida        | |   ▄▀▀▀▀▀▌\u001B[31m▄█▄\u001B[0m░\u001B[31m▄█▄\u001B[0m▐▀▀▀▀▀▄");
			System.out.println("| |                           | |  █▒▒▒▒▒▐░░░░▄░░░░▌▒▒▒▒▒█");
			System.out.println("| |   2. Nuevo personaje      | | ▐▒▒▒▒▒▒▒▌░░░░░░░▐▒▒▒▒▒▒▒▌");
			System.out.println("| |                           | | ▐▒▒▒▒▒▒▒█░▀▀▀▀▀░█▒▒▒▒▒▒▒▌");
			System.out.println("| |   0. Salir                | | ▐▒▒▒▒▒▒▒▒█▄▄▄▄▄█▒▒▒▒▒▒▒▒▌");
			System.out.println("| |                           | | ▐▒▒▒▒▐▒▒▒▒▒▒▒▒▒▒▒▒▐▒▒▒▒▒▌");
			System.out.println("| |___________________________| | ▐▒▒▒▒▒█▒▒▒▒▒▒▒▒▒▒▒█▒▒▒▒▒▌");            
			System.out.println("|                               | ▐▒▒▒▒▒▐▒▒▒▒▒▒▒▒▒▒▒▌▒▒▒▒▒▌");
			System.out.println("|      __                       | ▐▒▒▒▒▒▒▌▒▒▒▒▒▒▒▒▒▐▒▒▒▒▒▒▌");
			System.out.println("|   __|  |__            __      | ▐▒▒▒▒▒▒▌▄▄▄▄▄▄▄▄▄▐▒▒▒▒▒▒▌");
			System.out.println("|  |__    __|      __  |__|     | ▐▄▄▄▄▄▄▌▌███████▌▐▄▄▄▄▄▄▌");
			System.out.println("|     |__|        |__|          |  █▀▀▀▀█ ▌███▌███▌ █▀▀▀▀█");
			System.out.println("|                               |  ▐░░░░▌ ▌███▌███▌ ▐░░░░▌");
			System.out.println("|                               |   ▀▀▀▀  ▌███▌███▌  ▀▀▀▀");
			System.out.println("|          _________            |         ▌███▌███▌");
			System.out.println("|         |_________|           |         ▌███▌███▌");
			System.out.println("|            START              |       ▐▀▀▀██▌█▀▀▀▌");
			System.out.println("|_______________________________| ▒▒▒▒▒▒▐▄▄▄▄▄▄▄▄▄▄▌▒▒▒▒▒▒▒");
			System.out.print("\rElija una opción: ");
			opcion = leer.nextInt();
		} while (!(opcion >= 0 && opcion <= 2));  // Si no introduce un número entre 0 y 2, le seguiremos mostrando el menú hasta que lo haga.
		
		OpcionMenuPrincipal(opcion);
	}
	
	// Realizamos una acción u otra según la opción que introduzca.
	public static void OpcionMenuPrincipal(int opcion) {
		switch (opcion) {
		case 1:
			nueva_partida();
			break;
		case 2:
			nuevo_personaje();
			menuZombicide();
			break;
		case 0:
			break;
		}
	}
	
	public static void nueva_partida() {
		seleccionarJugadores();
		Partida partida = new Partida(jugadores_seleccionados);
		partida.initPartida();
	}
	
	public static void nuevo_personaje() {
		Scanner leer = new Scanner(System.in);
		if (jugadores.size() < 10) {  // En caso de que haya menos de 10 jugadores
			System.out.println("\rIntroduce el nombre del nuevo personaje:");
			String nombre = leer.nextLine();
			boolean personajeExiste = personajeYaExiste(nombre); // Verificamos que no haya ya un jugador con ese mismo nombre.

			if (personajeExiste == true) {
				System.out.println("\rEste jugador ya existe.");
				nuevo_personaje();
			} else {  // Si no existe, lo añadimos a nuestro ArrayList de jugadores.
				jugadores.add(new Jugador(nombre, 5, 5, true, true, new Arma()));
				System.out.println("\rEl jugador ha sido añadido a tu lista de jugadores!");
			}
		} else {
			System.out.println("\r¡El maximo de jugadores es 10. No puedes añadir mas personajes.");
		}
	}
	
	public static boolean personajeYaExiste(String nombre) {
		for (int i = 0; i < jugadores.size(); i++) {
			if (jugadores.get(i).getNombre().equalsIgnoreCase(nombre)) {
				return true;
			}
		}
		return false;
	}

	public static void init_personajes() {
		jugadores.add(new Jugador("James", 7, 7, true, true, new Arma("Mandoble", 2, 1, 4, 1)));
		jugadores.add(new Jugador("Marie", 5, 5, true, true, new Arma()));
		jugadores.add(new Jugador("Jaci", 5, 5, true, true, new Arma()));
	}

	public static void init_zombies() {
		zombies.add(new Caminante(true));
		zombies.add(new Gordo(true));
		zombies.add(new Corredor(true));
	}

	public static void init_objetos() {
		armas.add(new Arco("Arco largo", 1, 2, 3, 1));
		armas.add(new Hacha("Hacha doble", 2, 1, 3, 1));
		armas.add(new Hechizo("Bola de fuego", 1, 3, 4, 1));
		armas.add(new Espada("Espada corta", 1, 1, 4, 1));
	}

	// Recorremos el ArrayList de jugadores y mostramos los datos de cada uno.
	public static void todosJugadores() {
		for (int i = 0; i < jugadores.size(); i++) {
			System.out.println(i + "-->" + jugadores.get(i).toString());
		}
		System.out.println("");
	}

	public static ArrayList<Jugador> seleccionarJugadores() {
		Scanner leer = new Scanner(System.in);
		if (jugadores.size() == 3) {// Si solo hay tres jugadores creados, devolvemos estos, que serán nuestros jugadores seleccionados.
			jugadores_seleccionados = jugadores;
			return jugadores_seleccionados;
		} else {
			int num_jugadores;
			int num_seleccionados = 1;
			int jugador_seleccionado;
			do {
				System.out
						.println("\rCon cuantos jugadores quieres jugar? (El minimo de jugadores es 3, el maximo es 6)");
				num_jugadores = leer.nextInt();
			} while (!(num_jugadores >= 3 && num_jugadores <= jugadores.size() && num_jugadores <= 6));
			// Si no introduce un número entre 3 y 6, se le volverá a solicitar.
			do {
				do {
					System.out.println("\rSelecciona el jugador numero " + num_seleccionados + ":");
					System.out.println("Introduce el numero que corresponda al jugador que quieras seleccionar:");
					todosJugadores();
					jugador_seleccionado = leer.nextInt();// Mostramos a todos los jugadores.
				} while (!(jugador_seleccionado < jugadores.size() && jugador_seleccionado >= 0));
				// Si introduce un número que no corresponde a ninguna posición del ArrayList de jugadores, se le volverá a pedir.
				boolean jugador_ya_seleccionado = jugadorYaSeleccionado(jugador_seleccionado);
				if (jugador_ya_seleccionado == false) {// Verificamos que no se seleccione dos veces al mismo jugador.
					jugadores_seleccionados.add(new Jugador(jugadores.get(jugador_seleccionado).getNombre(),
							jugadores.get(jugador_seleccionado).getSalud(),
							jugadores.get(jugador_seleccionado).getMaxSalud(),
							jugadores.get(jugador_seleccionado).isVivo(),
							jugadores.get(jugador_seleccionado).haPasado(),
							jugadores.get(jugador_seleccionado).getArma()));
					num_seleccionados++;
				} else {
					System.out.println("\rEste jugador ya ha sido seleccionado. Selecciona otro jugador.");
				}
			} while (!(num_seleccionados > num_jugadores));
			return jugadores_seleccionados;
		}
	}

	// Recorremos el ArrayList de jugadores seleccionados. Si el usuario introduce un número que corresponde a la posición de un jugador seleccionado previamente, devolvemos verdadero. De lo contrario, devolvemos falso.
	public static boolean jugadorYaSeleccionado(int jugador_seleccionado) {
		for (int i = 0; i < jugadores_seleccionados.size(); i++) {
			if (jugadores_seleccionados.get(i).getNombre()
					.equalsIgnoreCase(jugadores.get(jugador_seleccionado).getNombre())) {
				return true;
			}
		}
		return false;
	}

	// Generamos un número aleatorio entre 0 y 100. Si es mayor que 40, devolvemos un arma aleatoria de nuestro ArrayList de armas.
	public static Arma getArma() {
		int porcentage = (int) (Math.random() * 100) + 1;
		if (porcentage > 40) {
			int arma_aleatoria = (int) (Math.random() * armas.size()) + 0;
			return armas.get(arma_aleatoria);
		} else {
			return null;
		}
	}
}

