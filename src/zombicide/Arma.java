package zombicide;

import java.util.ArrayList;

public class Arma {

	private String nombre;
	private int dano;
	private int alcance;
	private int acierto;
	protected int usos_habilidad_especial;

	
	//ARMA INICIAL
	public Arma() {
		nombre = "Daga";
		dano = 1;
		alcance = 1;
		acierto = 4;
		usos_habilidad_especial = 1;
	}

	public Arma(String nombre, int dano, int alcance, int acierto, int usos_habilidad_especial) {
		setNombre(nombre);
		setDano(dano);
		setAlcance(alcance);
		setAcierto(acierto);
		setUsos_habilidad_especial(usos_habilidad_especial);
	}

	//HABILIDAD ESPECIAL
	public int getUsos_habilidad_especial() {
		return usos_habilidad_especial;
	}
	
	public void setUsos_habilidad_especial(int usos_habilidad_especial) {
		this.usos_habilidad_especial = 0;
	}
	//NOMBRE
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	//DAÑO
	public int getDano() {
		return dano;
	}

	public void setDano(int dano) {
		this.dano = dano;
	}
	//ALCANCE
	public int getAlcance() {
		return alcance;
	}

	public void setAlcance(int alcance) {
		this.alcance = alcance;
	}
	//ACIERTO
	public int getAcierto() {
		return acierto;
	}

	public void setAcierto(int acierto) {
		this.acierto = acierto;
	}
	//INFORMACIÓN
	public String toString() {
		return "[" + getNombre() + ", dano=" + getDano() + ", alcance=" + getAlcance()+ ", acierto=" + getAcierto() + "]";
	}

	public ArrayList<Zombie> ataque_especial(ArrayList<Zombie> zombis) {
		return null;
	}
}
	
	
