package zombicide;

import java.util.ArrayList;

public class Corredor extends Zombie{
	// Constructor para un Corredor con parámetros personalizados
	public Corredor(String nombre, int salud, int maxSalud, boolean vivo, int movimiento, int dano, int tipo) {
		super(nombre, salud, maxSalud, vivo, movimiento, dano, tipo);
	}
	
	// Constructor para un Corredor simple (con valores por defecto)
	public Corredor(boolean vivo) {
		super("Corredor", 1, 1, vivo, 2, 1, 3);
	}
	
	// Quitamos todos los zombies tipo 3 (Corredor) de nuestro ArrayList de zombies enemigos.
	public ArrayList<Zombie> habilidad_especial(ArrayList<Zombie> zombies){
		for (int i = 0; i < zombies.size(); i++) {
			if (zombies.get(i).getTipo() == 3) {
				zombies.remove(i);
			}
		}
		return zombies;
	}
}
