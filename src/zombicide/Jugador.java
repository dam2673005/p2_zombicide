package zombicide;

import java.util.ArrayList;

public class Jugador extends Humanoide {
    private Arma arma;
    private boolean haPasado;
    protected ArrayList<Arma> armasDisponibles;

 // Constructor de la clase Jugador
    public Jugador(String nombre, int salud, int maxSalud, boolean vivo, boolean haPasado, Arma arma) {
    	super(nombre, salud, maxSalud, vivo);
    	setArma(arma);
    	setHaPasado(haPasado);
    	armasDisponibles = new ArrayList<Arma>();
    	armasDisponibles.add(arma);
    }

    // Métodos getter y setter para el arma
    public Arma getArma() {
        return arma;
    }
    
    public void setArma(Arma arma) {
    	this.arma = arma;
    }

    // Métodos getter y setter para el estado de haber pasado el turno
    public boolean haPasado() {
        return haPasado;
    }

    public void setHaPasado(boolean haPasado) {
        this.haPasado = haPasado;
    }
    
    // Método toString para representar el objeto Jugador como cadena de texto
    public String toString() {
    	return getNombre() + ", S = " + getSalud() + ", Arma = " + getArma();
    }
}