package zombicide;

import java.util.ArrayList;

public class Arco extends Arma {
	public Arco(String nombre, int dano, int alcance, int acierto, int usos_habilidad_especial) {
		super(nombre, dano, alcance, acierto, usos_habilidad_especial);
	}
	
	//FUNCION DE LA HABILIDAD ESPECIAL: ARCO (MATA 3 ZOMBIES CORREDORES)
	public ArrayList<Zombie> ataque_especial(ArrayList<Zombie> zombies) {
		int contador= 0;
		for (int i = 0; i < zombies.size() && contador < 1; i++) {
			if( zombies.get(i).getTipo() == 3) {
				zombies.remove(i);
				contador++;
			}
		}
		return zombies;
	}
}


