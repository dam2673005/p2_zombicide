package zombicide;

import java.util.ArrayList;

public class Espada extends Arma {
	public Espada(String nombre, int dano, int alcance, int acierto, int usos_habilidad_especial) {
		super(nombre, dano, alcance, acierto, usos_habilidad_especial);
	}
	//ELIMINA AL ZOMBIE QUE ESTE EN LA CASILLA QUE HAYA TOCADO EL NÚMERO ALEATORIO (solo zombies que esten en el arrayList)
	public ArrayList<Zombie> ataque_especial(ArrayList<Zombie> zombies) {
		int contador = 0;
		do {
			int zombie_aleatorio = (int) (Math.random() * zombies.size()) + 0;
			zombies.remove(zombie_aleatorio);
			contador++;
		} while ((!(contador < 2)));
		return zombies;
	}
}