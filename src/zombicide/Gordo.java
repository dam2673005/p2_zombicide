package zombicide;

import java.util.ArrayList;

public class Gordo extends Zombie{
	// Constructor para un Gordo con parámetros personalizados
	public Gordo(String nombre, int salud, int maxSalud, boolean vivo, int movimiento, int dano, int tipo) {
		super(nombre, salud, maxSalud, vivo, movimiento, dano, tipo);
	}

	// Constructor para un Gordo simple (con valores por defecto)
	public Gordo(boolean vivo) {
		super("Gordo", 2, 2, vivo, 1, 1, 2);
	}
	
	// Quitamos un zombie tipo 2 (Gordo) de nuestro ArrayList de zombies enemigos.
	public ArrayList<Zombie> habilidad_especial(ArrayList<Zombie> zombies) {
		int contador = 0;
		for (int i = 0; i < zombies.size() && contador < 1; i++) {
			if (zombies.get(i).getTipo() == 2) {
				zombies.remove(i);
				contador++;
			}
		}
		return zombies;
	}
}
