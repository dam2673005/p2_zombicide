package zombicide;

import java.util.ArrayList;

public class Zombie extends Humanoide{
	private int movimiento;
	private int dano;
	private int tipo;
	
	// Constructor
	public Zombie(String nombre, int salud, int maxSalud, boolean vivo, int movimiento, int dano, int tipo) {
		super(nombre, salud, maxSalud, vivo);
		setMovimiento(movimiento);
		setDano(dano);
		setTipo(tipo);
	}
	
	// Métodos getter y setter para el movimiento
	public int getMovimiento() {
		return movimiento;
	}
	
	public void setMovimiento(int movimiento) {
		this.movimiento = movimiento;
	}
	
	// Métodos getter y setter para el daño
	public int getDano() {
		return dano;
	}
	
	public void setDano(int dano) {
		this.dano = dano;
	}
	
	// Métodos getter y setter para el tipo
	public int getTipo() {
		return tipo;
	}
	
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	
	// Método toString para representar el objeto Zombie como cadena de texto
    public String toString() {
        return getNombre() + " movimiento = " + getMovimiento() + ", dano = " + getDano() + ", salud = " + getSalud();
    }
    
    // Método de habilidad especial (por ahora solo devuelve la lista de zombies)
    public ArrayList<Zombie> habilidad_especial(ArrayList<Zombie> zombies){
    	return zombies;
    }
}
